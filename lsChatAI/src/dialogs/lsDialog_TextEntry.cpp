#include "lsDialog_TextEntry.h"

lsDialog_TextEntry::lsDialog_TextEntry() :
	m_Label( "_Name", true ),
	m_ButtonBox( Gtk::Orientation::HORIZONTAL, 5 ),
	m_Button_OK( "_OK", true ),
	m_Button_Cancel( "_Cancel", true )
{
	set_destroy_with_parent(true);
	
	set_title( "Entry Dialog" );
	set_child( m_Grid );
	
	m_Grid.set_row_spacing(4);
	m_Grid.set_column_spacing(4);
	m_Grid.set_expand(true);
	
	m_Image.set_from_icon_name("dialog-question");
	m_Image.set_icon_size(Gtk::IconSize::LARGE);
	m_Grid.attach(m_Image, 0, 0, 1, 2);
	
	m_Grid.attach( m_Label, 1, 0 );
	m_Grid.attach( m_Entry, 2, 0 );
	m_Label.set_mnemonic_widget( m_Entry );
	m_Entry.set_hexpand(true);
	
	m_Grid.attach( m_ButtonBox, 0, 2, 3, 1 );
	m_ButtonBox.set_halign( Gtk::Align::END );
	m_ButtonBox.append( m_Button_OK );
	m_ButtonBox.append( m_Button_Cancel );
	m_ButtonBox.set_hexpand(true);
	
}

lsDialog_TextEntry::~lsDialog_TextEntry()
{
}


void lsDialog_TextEntry::buttons_clicked_connect( const sigc::slot<void(const Glib::ustring&)>& slot )
{
	m_Button_OK.signal_clicked().connect(sigc::bind( slot, "OK") );
	m_Button_Cancel.signal_clicked().connect(sigc::bind( slot, "Cancel") );
}

Glib::ustring lsDialog_TextEntry::get_entry()
{
	return m_Entry.get_text();
}


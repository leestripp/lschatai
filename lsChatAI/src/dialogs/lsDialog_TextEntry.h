#ifndef LSDIALOG_TEXTENTRY_H
#define LSDIALOG_TEXTENTRY_H

#include <iostream>

// gtkmm
#include <gtkmm.h>
// glibmm
#include <glibmm.h>

using namespace std;


class lsDialog_TextEntry : public Gtk::Window
{
public:
	lsDialog_TextEntry();
	virtual ~lsDialog_TextEntry();
	
	void buttons_clicked_connect( const sigc::slot<void(const Glib::ustring&)>& slot );
	Glib::ustring get_entry();
	
	void set_label( const string& val )
	{
		m_Label.set_text( val );
	}
	
private:
	Gtk::Grid m_Grid;
	Gtk::Image m_Image;
	Gtk::Label m_Label;
	Gtk::Entry m_Entry;
	Gtk::Box m_ButtonBox;
	Gtk::Button m_Button_OK;
	Gtk::Button m_Button_Cancel;
};

#endif // LSDIALOG_TEXTENTRY_H
#ifndef LSCHATAI_H
#define LSCHATAI_H

#include <filesystem>
#include <iostream>
#include <fstream>
#include <thread>
#include <mutex>
#include <regex>

// gtkmm
#include <gtkmm.h>
// lsConfig
#include <lsConfig/lsConfig.h>
// Model info
#include "lsModelInfo.h"
// dialogs.
#include "lsDialog_TextEntry.h"
// lsCurl
#include <lsCurl/lsCurl.h>

#include "lsLlamacpp.h"

using namespace std;

class lsChatAI : public Gtk::Box
{
public:
	lsChatAI();
	virtual ~lsChatAI();
	
	// direct
	void submit( const string& prompt );
	
	// Thread callbacks
	void add_text( const string& text );
	void add_link( const tuple<string, string>& link );
	void notify();
	void write();
	void write_links();
	void ingest_notify();
	
	// database.
	void ingest( const vector<string>& headers );
	void clear_db();
	void loadTree( const string& dir );
	// model download
	void model_download_update( lsThread *th );
	void model_download_done( lsThread *th );
	
	void set_MainWindow( Gtk::Window *val )
	{
		m_main_window = val;
	}
	
private:
	// buttons.
	void on_submit_clicked();
	void on_stop_clicked();
	
	// Menu Import
	void on_action_import_textdocuments();
	void on_action_import_webpage();
	void on_action_import_cleardata();
	// Menu Memory
	void on_action_memory_clear();
	
	// import dialogs
	void on_action_importdocs();
	void on_folder_dialog_response( int response_id, Gtk::FileChooserDialog* dialog );
	
	// combo
	void on_model_changed();
	// load text files.
	string loadFile( const string& file );
	// submit thread
	void on_notification_from_worker_thread();
	// Write buffer to TextView
	void write_text();
	// Write links to TextView
	void write_buffer_links();
	void loadModels( const string& path );;
	// ingest done
	void ingest_done();
	// model download done GLib.
	void modeldownlaod_done();
	
	// dialogs
	void on_dialog_webpage_response( const Glib::ustring& response );
	
	// main window pointer
	Gtk::Window *m_main_window;
	// Menu
	Gtk::PopoverMenuBar *m_MenuBar;
	
	Gtk::Box m_VBox;
	Gtk::ComboBoxText m_combo;
	Gtk::ScrolledWindow m_ScrolledWindow1;
	Gtk::TextView m_textview;
	// Temp
	Gtk::Scale m_scale;
	// user prompt
	Gtk::Box m_prompt_HBox;
	Gtk::Label m_prompt_label;
	Gtk::ScrolledWindow m_ScrolledWindow2;
	Gtk::TextView m_textinput;
	Gtk::Button m_submit;
	Gtk::Button m_stop;
	Gtk::Button m_clear_memory;
	
	// Dialogs
	lsDialog_TextEntry m_dialog_webpage;
	
	// models
	vector<lsModelInfo> m_models;
	// lsllamacpp
	lsLlamacpp m_ai;
	// download.
	lsCurl m_lscurl_download;
	ulong m_progress;
	// store model info for download thread.
	string m_current_model_name;
	string m_current_model_path;
	
	// Threads
	Glib::Dispatcher m_Dispatcher_notify;
	Glib::Dispatcher m_Dispatcher_write;
	Glib::Dispatcher m_Dispatcher_write_links;
	Glib::Dispatcher m_Dispatcher_ingest;
	Glib::Dispatcher m_Dispatcher_model_download;
	thread *m_WorkerThread;
	mutex m_write_mutex;
	// text buffer
	string m_write;
	// links
	vector<tuple<string, string>> m_links;
	
	// lsConfig
	lsConfig m_settings;
};

#endif // LSCHATAI_H


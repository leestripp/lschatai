#ifndef LSCHATAI_MENU_H
#define LSCHATAI_MENU_H

Glib::ustring lschatai_menu_info =
  "<interface>"
  "  <menu id='lschatai_menu'>"
  "    <submenu>"
  "      <attribute name='label' translatable='yes'>Import</attribute>"
  "      <section>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Text Documents</attribute>"
  "          <attribute name='action'>lschatai_menu.import_textdocuments</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Web Page</attribute>"
  "          <attribute name='action'>lschatai_menu.import_webpage</attribute>"
  "        </item>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Clear Imported Data</attribute>"
  "          <attribute name='action'>lschatai_menu.clear_imported_data</attribute>"
  "        </item>"
  "      </section>"
  "    </submenu>"
  
  "    <submenu>"
  "      <attribute name='label' translatable='yes'>Memory</attribute>"
  "      <section>"
  "        <item>"
  "          <attribute name='label' translatable='yes'>Clear Memory</attribute>"
  "          <attribute name='action'>lschatai_menu.memory_clear</attribute>"
  "        </item>"
  "      </section>"
  "    </submenu>"
  
  "  </menu>"
  "</interface>";

#endif // LSCHATAI_MENU_H


#ifndef LSMODELINFO_H
#define LSMODELINFO_H

#include <iostream>
#include <vector>

using namespace std;

class lsModelInfo
{
public:
	lsModelInfo();
	
	// model
	string 			m_name;
	string 			m_path;
	string 			m_download;
	int 			m_context_size;
	int 			m_prompt_type;
	// memory
	int				m_max_memory;
	// chunkyDB
	bool			m_use_chunkyDB;
	int				m_max_db_chunks;
	int				m_chunk_size;
};

#endif // LSMODELINFO_H


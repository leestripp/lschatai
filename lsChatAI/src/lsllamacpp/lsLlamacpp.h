#ifndef LSLLAMACPP_H
#define LSLLAMACPP_H

// gtkmm
#include <gtkmm.h>

#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <cstdlib>
#include <algorithm>
#include <format>

// llama.cpp
#include <common.h>
#include <llama.h>

// Chunky DB
#include <lsChunkyDB/lsChunkyDB.h>

// Agents
#include "lsAgents.h"

using namespace std;

// protos
class lsChatAI;

// prompt types
const int pt_none 		= 0;
const int pt_llama 		= 1;
const int pt_mistral 	= 2;

class lsLlamacpp
{
public:
	lsLlamacpp();
	virtual ~lsLlamacpp();
	
	int submit( lsChatAI *chat );
	int submit_chunk( lsChatAI *chat );
	void clear_memory();
	void clear_context();
	void set_model( const string& path, const string& model_name );
	// database.
	void ingest( lsChatAI *chat );
	void clear_db();
	bool chunkyDB_empty();
	
	// get set
	
	void set_prompt( const string& val )
	{
		m_prompt = val;
	}
	
	void set_context_size( int val )
	{
		m_context_size = val;
	}
	
	void set_prompt_type( int val )
	{
		m_prompt_type = val;
	}
	
	void set_temperature( float val )
	{
		m_temperature = val;
	}
	
	void stop()
	{
		m_stop = true;
	}
	
	void use_chunkyDB( bool val )
	{
		m_use_chunkyDB = val;
	}
	
	void set_max_memory( int val )
	{
		m_max_memory = val;
	}
	
	void set_max_db_chunks( int val )
	{
		m_max_db_chunks = val;
	}
	
	void set_chunk_size( int val )
	{
		m_chunk_size = val;
	}
	
	// Ingest
	
	void set_ingest_data( const vector<string>& val )
	{
		m_data = val;
	}
	
private:
	void build_prompt( lsChatAI *chat );
	// init
	void init_model();
	void cleanup();
	
	// model settings
	gpt_params m_params;
	llama_model *m_model;
	llama_context *m_ctx;
	string m_model_path;
	string m_prompt;
	ulong m_context_size;
	int m_past;					// tokens used.
	int m_prompt_type;
	string m_build_prompt;
	float m_temperature;
	bool m_stop;
	
	// Agents
	lsAgents m_agents;
	
	// ingest data
	vector<string> m_data;
	// Chunky DB
	lsChunkyDB m_chunkyDB;
	bool m_use_chunkyDB;
	int m_max_db_chunks;
	ulong m_chunk_size;
	
	// used in building the prompt.
	vector<string> m_context_history;
	
	// short term memory
	lsChunkyDB m_short_memory;
	int m_max_short_memory;
	// long term memory
	lsChunkyDB m_memory;
	int m_max_memory;
};

#endif // LSLLAMACPP_H

#include "lsChatAI.h"
// menu
#include "menus/lschatai_menu.h"

// Web Scrape
#include <lsWebScrape/lsWebScrape.h>

lsChatAI::lsChatAI() :
	m_VBox(Gtk::Orientation::VERTICAL),
	m_prompt_HBox(Gtk::Orientation::HORIZONTAL, 5)
{
	m_WorkerThread = nullptr;
	m_main_window = nullptr;
	
	// Text Entry Dialog
	m_dialog_webpage.set_title( "Enter webpage link" );
	m_dialog_webpage.set_label( "URL :" );
	m_dialog_webpage.set_default_size( 250, 100 );
	m_dialog_webpage.set_modal();
	m_dialog_webpage.set_hide_on_close();
	m_dialog_webpage.buttons_clicked_connect( sigc::mem_fun(*this, &lsChatAI::on_dialog_webpage_response) );
	
	//****************
	// UI Builder
	auto m_refBuilder = Gtk::Builder::create();
	
	// lschatai menu
	m_refBuilder->add_from_string( lschatai_menu_info );
	auto gmenu = m_refBuilder->get_object<Gio::Menu>( "lschatai_menu" );
	m_MenuBar = Gtk::make_managed<Gtk::PopoverMenuBar>( gmenu );
	m_VBox.append( *m_MenuBar );
	
	// Actions
	auto refActionGroup = Gio::SimpleActionGroup::create();
	// Import
	refActionGroup->add_action( "import_textdocuments", sigc::mem_fun( *this, &lsChatAI::on_action_import_textdocuments ) );
	refActionGroup->add_action( "import_webpage", sigc::mem_fun( *this, &lsChatAI::on_action_import_webpage ) );
	refActionGroup->add_action( "clear_imported_data", sigc::mem_fun( *this, &lsChatAI::on_action_import_cleardata ) );
	// Memory
	refActionGroup->add_action( "memory_clear", sigc::mem_fun( *this, &lsChatAI::on_action_memory_clear ) );
	// Add group
	insert_action_group( "lschatai_menu", refActionGroup );
	
	//**************
	// Widgets.
	
	//Add TextView's to ScrolledWindows
	m_ScrolledWindow1.set_child( m_textview );
	m_ScrolledWindow2.set_child( m_textinput );
	m_ScrolledWindow2.set_size_request( -1, 200 );
	
	// user prompt label
	m_prompt_label.set_text( "User prompt" );
	// spacer label.
	auto spacer_label = Gtk::make_managed<Gtk::Label>("");
	spacer_label->set_hexpand( true );
	spacer_label->set_vexpand( false );
	
	// user prompt layout
	m_prompt_HBox.append( m_prompt_label );
	m_prompt_HBox.append( *spacer_label );
	
	// Courage (Temperature) slider
	auto temp_hbox = Gtk::make_managed<Gtk::Box>( Gtk::Orientation::HORIZONTAL, 5 );
	auto temp_label = Gtk::make_managed<Gtk::Label>( "Courage (temp): " );
	temp_hbox->append( *temp_label );
	temp_hbox->append( m_scale );
	m_scale.set_hexpand( true );
	m_scale.set_range( 0.0f, 1.0f );
	m_scale.set_value_pos( Gtk::PositionType::BOTTOM );
	m_scale.set_value( 0.7f );
	
	// Layout
	m_VBox.set_margin( 10 );
	m_VBox.append( m_combo );
	m_VBox.append( m_ScrolledWindow1 );
	m_VBox.append( *temp_hbox );
	m_VBox.append( m_prompt_HBox );
	m_VBox.append( m_ScrolledWindow2 );
	m_VBox.set_margin( 10 );
	
	// Set m_textview defaults
	m_textview.set_expand();
	m_textview.property_wrap_mode() = Gtk::WrapMode::WORD;
	m_textview.property_justification() = Gtk::Justification::LEFT;
	m_textview.set_editable( false );
	
	// Set m_textinput defaults
	m_textinput.set_size_request( -1, 200 );
	m_textinput.set_hexpand( true );
	m_textinput.set_vexpand( false );
	m_textinput.property_wrap_mode() = Gtk::WrapMode::WORD;
	m_textinput.property_justification() = Gtk::Justification::LEFT;
	
	// Buttons
	auto b_hbox = Gtk::make_managed<Gtk::Box>( Gtk::Orientation::HORIZONTAL, 5 );
	b_hbox->append( m_submit );
	b_hbox->append( m_stop );
	b_hbox->append( m_clear_memory );
	m_VBox.append( *b_hbox );
	
	// Submit Button
	auto label = Gtk::make_managed<Gtk::Label>("Submit");
	label->set_expand(true);
	//Put them in a Box:
	auto hbox = Gtk::make_managed<Gtk::Box>(Gtk::Orientation::HORIZONTAL, 5);
	hbox->append( *label );
	//And put that Box in the button:
	m_submit.set_child( *hbox );
	m_submit.set_hexpand( true );
	m_submit.set_vexpand( false );
	
	// Stop Button
	auto label_stop = Gtk::make_managed<Gtk::Label>("Stop");
	label_stop->set_expand(true);
	//Put it in a Box:
	auto hbox_stop = Gtk::make_managed<Gtk::Box>(Gtk::Orientation::HORIZONTAL, 5);
	hbox_stop->append( *label_stop );
	//And put that Box in the button:
	m_stop.set_child( *hbox_stop );
	m_stop.set_hexpand( true );
	m_stop.set_vexpand( false );
	
	// m_clear_memory Button
	auto label_ch = Gtk::make_managed<Gtk::Label>( "Clear Memory" );
	label_ch->set_expand( true );
	auto hbox_ch = Gtk::make_managed<Gtk::Box>( Gtk::Orientation::HORIZONTAL, 5 );
	hbox_ch->append( *label_ch );
	m_clear_memory.set_child( *hbox_ch );
	m_clear_memory.set_hexpand( true );
	m_clear_memory.set_vexpand( false );
	
	append( m_VBox );
	
	// model downloads.
	m_progress = 0;
	// only for user thread reference.
	m_lscurl_download.set_type( 0 );
	// connect
	m_lscurl_download.notify_done.connect( sigc::mem_fun(*this, &lsChatAI::model_download_done) );
	m_lscurl_download.notify_update.connect( sigc::mem_fun(*this, &lsChatAI::model_download_update) );
	// Get Home directory.
	string home_dir = Glib::get_home_dir();
	cout << "Home directory: " << home_dir << endl;
	// Load Models
	loadModels( home_dir );
	
	// Signals
	m_submit.signal_clicked().connect( sigc::mem_fun( *this, &lsChatAI::on_submit_clicked ) );
	m_stop.signal_clicked().connect( sigc::mem_fun( *this, &lsChatAI::on_stop_clicked ) );
	m_clear_memory.signal_clicked().connect( sigc::mem_fun( *this, &lsChatAI::on_action_memory_clear ) );
	// Models
	m_combo.signal_changed().connect( sigc::mem_fun( *this, &lsChatAI::on_model_changed ) );
	
	// Connect the handlers to the dispatchers.
	m_Dispatcher_notify.connect( sigc::mem_fun( *this, &lsChatAI::on_notification_from_worker_thread ) );
	m_Dispatcher_write.connect( sigc::mem_fun( *this, &lsChatAI::write_text ) );
	m_Dispatcher_write_links.connect( sigc::mem_fun( *this, &lsChatAI::write_buffer_links ) );
	m_Dispatcher_ingest.connect( sigc::mem_fun( *this, &lsChatAI::ingest_done ) );
	// Download done
	m_Dispatcher_model_download.connect( sigc::mem_fun( *this, &lsChatAI::modeldownlaod_done ) );
}

lsChatAI::~lsChatAI()
{
}

//******************
// direct submit

void lsChatAI::submit( const string& prompt )
{
	// Disable the buttons
	m_clear_memory.set_sensitive( false );
	m_submit.set_sensitive( false );
	
	// Clear old data.
	m_write = string();
	m_links.clear();
	
	// Set Temperature
	m_ai.set_temperature( m_scale.get_value() );
	// Set prompt
	m_ai.set_prompt( prompt );
	
	// Display prompt
	add_text( "\n### Direct Prompt\n" );
	add_text( prompt );
	write();
	
	// Start a new AI thread.
	m_WorkerThread = new std::thread( [this] { m_ai.submit( this ); } );
}

//**********************
// Load Model Configs.

void lsChatAI::loadModels( const string& path )
{
	cout << "Loading Llama Model Configs (.lmc)" << endl;
	
	string directory = path + "/.lsChatAI";
	
	if(! filesystem::exists( directory )  )
	{
		cout << "ERROR : No ~/.lsChatAI folder found..." << endl;
		return;
	}
	
	// Recursively list all files in the directory
	vector<string> model_files;
	for( const auto& entry : filesystem::recursive_directory_iterator( directory ) )
	{
		auto status = entry.status();
        if(! entry.is_directory() )
		{
			const auto dirStr = entry.path().string();
			const auto filenameStr = entry.path().filename().string();
			const auto extStr = entry.path().extension().string();
			
			// llama model config
			if( extStr == ".lmc" )
			{
				// Print the file name and path
				cout << "File : " << filenameStr;
				cout << "  Ext : " << extStr;
				cout << "  Path : " << dirStr << endl;
				
				// store for sort.
				model_files.push_back( dirStr );
				
			} // if config file
        } // if not dir
    } // recursive scan
	
	// Sort by filename.
	 sort( model_files.begin(), model_files.end());
	
	// Load model config files.
	if(! model_files.empty() )
	{
		for( const string& modelfile : model_files )
		{
			lsModelInfo m;
			string value;
			
			// Load data
			if(! m_settings.load( modelfile ) ) continue;
			// debug
			m_settings.print();
			
			// name
			value = m_settings.get_value( "name" );
			if(! value.empty() )
			{
				m.m_name = value;
			}
			
			// model
			value = m_settings.get_value( "model" );
			if(! value.empty() )
			{
				m.m_path = directory + "/" + value;
			}
			
			// download
			value = m_settings.get_value( "download" );
			if(! value.empty() )
			{
				m.m_download = value;
			}
			
			// context_size
			value = m_settings.get_value( "context_size" );
			if(! value.empty() )
			{
				m.m_context_size = stoi( value );
			}
			
			// prompt_type
			value = m_settings.get_value( "prompt_type" );
			if(! value.empty() )
			{
				m.m_prompt_type = stoi( value );
			}
			
			// max_memory
			value = m_settings.get_value( "max_memory" );
			if(! value.empty() )
			{
				m.m_max_memory = stoi( value );
			}
			
			// max_db_chunks
			value = m_settings.get_value( "max_db_chunks" );
			if(! value.empty() )
			{
				m.m_max_db_chunks = stoi( value );
			}
			
			// use_chunkyDB
			value = m_settings.get_value( "use_chunkyDB" );
			if(! value.empty() )
			{
				if( value == "true" )
				{
					m.m_use_chunkyDB = true;
				} else
				{
					m.m_use_chunkyDB = false;
				}
			}
			
			// chunk_size
			value = m_settings.get_value( "chunk_size" );
			if(! value.empty() )
			{
				m.m_chunk_size = stoi( value );
			}
			
			// Add model info to the List.
			m_models.push_back( m );
			
		} // for model_files
	}
	
	// Add models to Combo dropdown
	for( const auto& model : m_models )
	{
		m_combo.append( model.m_name );
	}
	// Set the default model.
	m_combo.set_active(0);
	
	// load model.
	on_model_changed();
}

void lsChatAI::on_model_changed()
{
	// Disable buttons
	m_clear_memory.set_sensitive( false );
	m_submit.set_sensitive( false );
	m_combo.set_sensitive( false );
	m_MenuBar->set_sensitive( false );
	
	// clear display
	auto input_buffer = m_textinput.get_buffer();
	if( input_buffer ) input_buffer->set_text("");
	auto output_buffer = m_textview.get_buffer();
	if( output_buffer ) output_buffer->set_text("");
	
	string combo_model = m_combo.get_active_text();
	for( auto& model : m_models )
	{
		if( combo_model == model.m_name )
		{
			m_ai.set_context_size( model.m_context_size );
			m_ai.set_prompt_type( model.m_prompt_type );
			m_ai.set_max_memory( model.m_max_memory );
			m_ai.set_max_db_chunks( model.m_max_db_chunks );
			m_ai.use_chunkyDB( model.m_use_chunkyDB );
			m_ai.set_chunk_size( model.m_chunk_size );
			// Store model info for download thread.
			m_current_model_name = model.m_name;
			m_current_model_path = model.m_path;
			
			// download model if needed.
			// model.m_path : contains the full path to the model file.
			if(! filesystem::exists( model.m_path ) )
			{
				m_progress = 0;
				
				// Tell user.
				add_text( "\n### Downloading Model\n" );
				write();
				
				m_lscurl_download.m_mode = LSM_DOWNLOAD;
				m_lscurl_download.m_url = model.m_download;
				m_lscurl_download.m_filename = model.m_path;
				// start thread.
				m_lscurl_download.start();
			} else
			{
				// Set and Load model
				m_ai.set_model( m_current_model_path, m_current_model_name );
				
				// Activate buttons.
				m_clear_memory.set_sensitive( true );
				m_submit.set_sensitive( true );
				m_combo.set_sensitive( true );
				m_MenuBar->set_sensitive( true );
			}
			break;
		}
	}
}

void lsChatAI::on_submit_clicked()
{
	// Disable the buttons
	m_clear_memory.set_sensitive( false );
	m_submit.set_sensitive( false );
	
	// Clear old data.
	m_write = string();
	m_links.clear();
	
	// Set Temperature
	m_ai.set_temperature( m_scale.get_value() );
	
	// buffers
	auto input_buffer = m_textinput.get_buffer();
	// User prompt
	string output = input_buffer->get_text();
	m_ai.set_prompt( output );
	input_buffer->set_text( "" );
	
	// Display prompt
	add_text( "\n### User\n" );
	add_text( output );
	write();
	
	// Start a new AI thread.
	m_WorkerThread = new std::thread( [this] { m_ai.submit( this ); } );
}

// Thread adds text here.
void lsChatAI::add_text( const string& text )
{
	// lock the mutex
	m_write_mutex.lock();
   m_write += text;
   m_write_mutex.unlock();
}

void lsChatAI::add_link( const tuple<string, string>& link )
{
	// lock the mutex
	m_write_mutex.lock();
	m_links.push_back( link );
	m_write_mutex.unlock();
}


// ready to write buffer.
void lsChatAI::write_text()
{
	// lock the mutex
	m_write_mutex.lock();
	auto output_buffer = m_textview.get_buffer();
	output_buffer->insert( output_buffer->end(), m_write );
	m_write = string();
	m_write_mutex.unlock();
	
	// Scroll the view to the end of the buffer
	auto endMark = output_buffer->create_mark( output_buffer->end(), false );
	m_textview.scroll_to( endMark );
	output_buffer->delete_mark( endMark );
}

// ready to add linkbuttons to buffer.
void lsChatAI::write_buffer_links()
{
	// lock the mutex
	m_write_mutex.lock();
	auto output_buffer = m_textview.get_buffer();
	// title
	output_buffer->insert( output_buffer->end(), "\n\n### Search results." );
	
	// Add Gtk::LinkButton's.
	for( const auto& link : m_links )
	{
		// debug
		cout << "Title: " << get<0>(link) << ", url: " <<  get<1>(link) << endl;
		
		// new line.
		output_buffer->insert( output_buffer->end(), "\n" );
		output_buffer->insert_markup( output_buffer->end(),  get<0>(link) );
		output_buffer->insert( output_buffer->end(), "\n" );
		
		// create anchor
		auto refAnchor = output_buffer->create_child_anchor( output_buffer->end() );
		// create link button. ( url, label )
		auto linkbutton = Gtk::make_managed<Gtk::LinkButton>( get<1>(link), "More Info..." );
		m_textview.add_child_at_anchor( *linkbutton, refAnchor );
	}
	m_links.clear();
	m_write_mutex.unlock();
	
	// Scroll the view to the end of the buffer
	auto endMark = output_buffer->create_mark( output_buffer->end(), false );
	m_textview.scroll_to( endMark );
	output_buffer->delete_mark( endMark );
}

// Emits

// Thread calls this when its done.
void lsChatAI::notify()
{
	// trigger call to - on_notification_from_worker_thread()
	m_Dispatcher_notify.emit();
}

// Thread calls this when its written to the buffer.
void lsChatAI::write()
{
	// trigger call to - on_notification_from_worker_thread()
	m_Dispatcher_write.emit();
}

// Thread calls this when its added all links.
void lsChatAI::write_links()
{
	// trigger call to - on_notification_from_worker_thread()
	m_Dispatcher_write_links.emit();
}

void lsChatAI::ingest_notify()
{
	// trigger call to - ingest_done
	m_Dispatcher_ingest.emit();
}

// main AI thread.

void lsChatAI::on_notification_from_worker_thread()
{
	if( m_WorkerThread )
	{
		if( m_WorkerThread->joinable() ) m_WorkerThread->join();
		delete m_WorkerThread;
		m_WorkerThread = nullptr;
	}
	
	// Enable buttons
	m_submit.set_sensitive( true );
	m_clear_memory.set_sensitive( true );
}

void lsChatAI::ingest_done()
{
	if( m_WorkerThread )
	{
		if( m_WorkerThread->joinable() ) m_WorkerThread->join();
		delete m_WorkerThread;
		m_WorkerThread = nullptr;
	}
	
	// Tell user its done.
	add_text( "### Documents added to database...\n" );
	write();
	
	m_ai.use_chunkyDB( true );
	
	// Enable buttons
	m_submit.set_sensitive( true );
	m_clear_memory.set_sensitive( true );
}

void lsChatAI::on_stop_clicked()
{
	// Stop the AI.
	m_ai.stop();
}

// Menu Memory

void lsChatAI::on_action_memory_clear()
{
	// Clears Long and Short term memory.
	m_ai.clear_memory();
	
	// clear UI elements.
	m_textview.get_buffer()->set_text( "" );
	m_textinput.get_buffer()->set_text( "" );
}

// database.

void lsChatAI::ingest( const vector<string>& headers )
{
	// store for thread.
	m_ai.set_ingest_data( headers );
	
	// Disable the buttons
	m_clear_memory.set_sensitive( false );
	m_submit.set_sensitive( false );
	
	// Start a new AI thread.
	m_WorkerThread = new std::thread( [this] { m_ai.ingest( this ); } );
}

void lsChatAI::clear_db()
{
	m_ai.clear_db();
}


//*******************
// Import Actions.

void lsChatAI::on_action_import_textdocuments()
{
	on_action_importdocs();
}

void lsChatAI::on_action_import_webpage()
{
	// Get root window
	Gtk::Root *root = get_root();
	Gtk::Window *window = dynamic_cast<Gtk::Window*>(root);
	m_dialog_webpage.set_transient_for( *window );
	
	// Get url form user.
	m_dialog_webpage.set_visible( true );
}

void lsChatAI::on_action_import_cleardata()
{
	clear_db();
}


//*******************
// Import docs dialog.

void lsChatAI::on_action_importdocs()
{
	// Is main window set.
	if(! m_main_window ) return;
	
	auto dialog = new Gtk::FileChooserDialog( "Choose (.txt) Documents folder", Gtk::FileChooser::Action::SELECT_FOLDER );
	
	dialog->set_transient_for(*m_main_window);
	dialog->set_modal(true);
	dialog->signal_response().connect( sigc::bind( sigc::mem_fun( *this, &lsChatAI::on_folder_dialog_response), dialog ) );
	
	//Add response buttons to the dialog:
	dialog->add_button("_Cancel", Gtk::ResponseType::CANCEL);
	dialog->add_button("Select", Gtk::ResponseType::OK);
	
	//Show the dialog
	dialog->show();
}

void lsChatAI::on_folder_dialog_response( int response_id, Gtk::FileChooserDialog* dialog )
{
	// Handle the response:
	switch( response_id )
	{
		case Gtk::ResponseType::OK:
		{
			// debug
			cout << "Folder selected : " << dialog->get_file()->get_path() << endl;
			
			// load docs tree
			loadTree( dialog->get_file()->get_path() );
			m_ai.use_chunkyDB( true );
			break;
		}
		
		case Gtk::ResponseType::CANCEL:
		{
			// debug
			// cout << "Cancel clicked." << endl;
			break;
		}
		
		default:
		{
			cout << "Unexpected button clicked." << endl;
			break;
		}
	}
	delete dialog;
}


//*******************
// Load docs.
// chunk_size : number of characters.
// Load any .txt files in the directory and all subdirectories.
// Maybe more files supported in the future.

void lsChatAI::loadTree( const string& dir )
{
	// Tell user
	add_text( "### Loading Documents...\n" );
	write();
	
	vector<string> headers;
	
	// recursive load.
	for( const auto& p: filesystem::recursive_directory_iterator( dir ) )
	{
		if(! filesystem::is_directory(p) )
		{
			string filename, path, ext;
			
			ext = p.path().extension();
			filename = p.path().filename();
			path = p.path();
			
			if( ext == ".txt" )
			{
				// load file
				string contents;
				cout << "chunkyDB - Processing file: " << path << endl;
				contents = loadFile( path );
				
				// Add it
				if(! contents.empty() ) headers.push_back( contents );
				
			} // if .txt file.
		} // if ! is_directory
	} // for recursive dir
	
	// Ingest
	ingest( headers );
}

string lsChatAI::loadFile( const string& file )
{
	stringstream ss;
	
	ifstream myfile( file );
	if( myfile.is_open() )
	{
		ss << myfile.rdbuf() << endl;
	}
	return ss.str();
}

// dialog responces

void lsChatAI::on_dialog_webpage_response( const Glib::ustring& response )
{
	m_dialog_webpage.set_visible( false );
	
	if( response == "OK" )
	{
		string url = m_dialog_webpage.get_entry();
		if(! url.empty() )
		{
			// Scrape webpage.
			lsWebScrape scrape;
			
			// page contents
			cout << "### Scrape Page : " << url << endl;
			string result = scrape.plain_text( url );
			
			// Add to database.
			if(! result.empty() )
			{
				// create data list.
				vector<string> data;
				data.push_back( result );
				// call threaded ingest.
				ingest( data );
				m_ai.use_chunkyDB( true );
			}
			
		} else
		{
			cout << "NOTE: empty url" << endl;
		}
	}// if response
}

//********************
// model download

void lsChatAI::model_download_update( lsThread *th )
{
	m_progress++;
	if( m_progress % 10000 == 0 )
	{
		lsCurl *lscurl = (lsCurl *)th;
		stringstream ss;
		double perc = ((double)lscurl->m_download_now/(double)lscurl->m_download_total)*100.0;
		
		ss << setprecision(2) << fixed;
		ss << "Down: % " << perc << ", (" << lscurl->m_download_now;
		ss << " of " << lscurl->m_download_total << ")" << endl;
		add_text( ss.str() );
		write();
	}
}

void lsChatAI::model_download_done( lsThread *th )
{
	// emit signal to safely join the thread.
	m_Dispatcher_model_download.emit();
	
	// Set and Load model
	m_ai.set_model( m_current_model_path, m_current_model_name );
	// Activate buttons.
	m_clear_memory.set_sensitive( true );
	m_submit.set_sensitive( true );
	m_combo.set_sensitive( true );
	m_MenuBar->set_sensitive( true );
	
	cout << "[DONE]" << endl;
	add_text( "\n### Model Downloaded.\n" );
	write();
}

// Glib Dispatcher.

void lsChatAI::modeldownlaod_done()
{
	// Join.
	m_lscurl_download.join();
}


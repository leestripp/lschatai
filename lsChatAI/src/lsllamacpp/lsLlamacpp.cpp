#include "lsLlamacpp.h"
#include "lsChatAI.h"

// Agents
#include "lsAgent_DateTime.h"
#include "lsAgent_Search.h"
#include "lsAgent_Weather.h"


lsLlamacpp::lsLlamacpp()
{
	// defaults
	m_context_size = 4096;
	m_prompt_type = pt_none;
	m_temperature = 0.7;
	m_stop = false;
	m_use_chunkyDB = false;
	
	m_ctx = NULL;
	m_model = NULL;
	
	// data and memory, Chunky DB
	// init done in set_model()
	m_max_db_chunks = 2;
	// long term memory
	m_max_memory = 1;
	// short term memory
	m_max_short_memory = 1;
	
    // init backend
	cout << "init llama backend..." << endl;
    llama_backend_init();
	
	// agents
	cout << "Adding Agents..." << endl;
	lsAgent_DateTime *dt = new lsAgent_DateTime( &m_agents );
	m_agents.add_agent( dt );
	lsAgent_Search *search = new lsAgent_Search( &m_agents );
	m_agents.add_agent( search );
	lsAgent_Weather *weather = new lsAgent_Weather( &m_agents );
	m_agents.add_agent( weather );
}

lsLlamacpp::~lsLlamacpp()
{
	cleanup();
	llama_backend_free();
}

void lsLlamacpp::build_prompt( lsChatAI *chat )
{
	string temp;
	
	// cleanup old data.
	m_context_history.clear();
	
	// Long term Momory
	// Holds past Agent output for now.
	vector<lsChunk> memories;
	memories = m_memory.find_chunks( m_prompt, m_max_memory );
	for( const auto& memory : memories )
	{
		// debug
		chat->add_text( "\n### Memory ID : " + to_string(memory.m_id) );
		chat->write();
		
		// Fomat chunk
		switch( m_prompt_type )
		{
			// Llama 3.1
			case pt_llama:
			{
				temp = m_memory.decrypt_chunk( memory, true );
				temp +=  "\n";
				m_context_history.insert( m_context_history.begin(), temp );
				break;
			}
			
			// mistral
			case pt_mistral:
			{
				temp  = "\n<s>[INST] context: ";
				// string lsChunkyDB::decrypt_chunk( const lsChunk& chunk, bool remove_header=false )
				temp += m_memory.decrypt_chunk( memory, true );
				temp +=  " [/INST]\n";
				temp +=  "Thanks.</s>";
				m_context_history.insert( m_context_history.begin(), temp );
				break;
			}
			
			// straight text
			case pt_none:
			{
				temp = m_memory.decrypt_chunk( memory, true );
				temp += "\n";
				m_context_history.insert( m_context_history.begin(), temp );
				break;
			}
		}// switch
	}
	temp = string();
	
	// Short term Momory
	// holds user prompt and AI response.
	vector<lsChunk> short_memories;
	short_memories = m_short_memory.find_chunks( m_prompt, m_max_short_memory );
	for( const auto& short_memory : short_memories )
	{
		// debug
		chat->add_text( "\n### Short Memory ID : " + to_string(short_memory.m_id) );
		chat->write();
		
		// string lsChunkyDB::decrypt_chunk( const lsChunk& chunk, bool remove_header=false )
		temp = m_short_memory.decrypt_chunk( short_memory, true );
		
		m_context_history.push_back( temp );
	}
	temp = string();
	
	// Chunky DB
	// Holds imported data, docs and webpages.
	if( m_use_chunkyDB )
	{
		// Find Chunks.
		vector<lsChunk> chunks;
		chunks = m_chunkyDB.find_chunks( m_prompt, m_max_db_chunks );
		for( const auto& chunk : chunks )
		{
			// debug
			chat->add_text( "\n### Reference ID: " + to_string(chunk.m_id) + " : " + chunk.m_reference );
			chat->write();
			
			// Fomat chunk
			switch( m_prompt_type )
			{
				// Llama 3.1
				case pt_llama:
				{
					temp = m_chunkyDB.decrypt_chunk( chunk, true );
					temp +=  "\n";
					m_context_history.insert( m_context_history.begin(), temp );
					break;
				}
				
				// mistral
				case pt_mistral:
				{
					temp  = "\n<s>[INST] context: ";
					temp += m_chunkyDB.decrypt_chunk( chunk, true );
					temp +=  " [/INST]\n";
					temp +=  "Thanks.</s>";
					m_context_history.insert( m_context_history.begin(), temp );
					break;
				}
				
				// straight text
				case pt_none:
				{
					temp = m_chunkyDB.decrypt_chunk( chunk, true );
					temp += "\n";
					m_context_history.insert( m_context_history.begin(), temp );
					break;
				}
			}// switch
		}
	} // m_use_chunkyDB
	temp = string();
	
	// Agents
	m_agents.run( m_prompt );
	if( m_agents.m_has_links )
	{
		// links
		for( const auto& link : m_agents.m_links )
		{
			// link buttons
			chat->add_link( link );
			// debug
			// cout << "Title: " << get<0>(link) << ", url: " <<  get<1>(link) << endl;
		}
		chat->write_links();
	}
	if(! m_agents.m_agent_responses.empty() )
	{
		string all_responses;
		for( const auto& response : m_agents.m_agent_responses )
		{
			// Add to buffer
			all_responses += response;
			all_responses += "\n";
		}
		
		// Fomat response
		switch( m_prompt_type )
		{
			// Llama 3.1
			case pt_llama:
			{
				temp  = "\n";
				temp += all_responses;
				m_context_history.push_back( temp );
				break;
			}
			
			// mistral
			case pt_mistral:
			{
				temp  = "\n<s>[INST] agent: ";
				temp += all_responses;
				temp +=  " [/INST]\n";
				temp +=  "Thanks Agent.</s>";
				m_context_history.push_back( temp );
				break;
			}
			
			// straight text
			case pt_none:
			{
				temp = all_responses + "\n";
				m_context_history.push_back( temp );
				break;
			}
		}// switch
		
		// store all_responses
		m_memory.insert( all_responses, m_chunk_size );
	}
	
	// Add User prompt at the end.
	// debug
	// cout << __func__ << " - prompt: " << m_prompt << endl;
	switch( m_prompt_type )
	{
		// Llama 3.1
		case pt_llama:
		{
			temp  = m_prompt;
			temp +=  "\n";
			m_context_history.push_back( temp );
			break;
		}
		
		// mistral
		case pt_mistral:
		{
			temp  = "\n<s>[INST] ";
			temp += m_prompt;
			temp +=  " [/INST]</s>\n";
			m_context_history.push_back( temp );
			break;
		}
		
		// straight text
		case pt_none:
		{
			temp = m_prompt + "\n";
			m_context_history.push_back( temp );
			break;
		}
	}// switch
	
	// **************************
	// Create final prompt.
	m_build_prompt = string();
	switch( m_prompt_type )
	{
		// Llama 3.1
		// BOS token = <|begin_of_text|>
		// EOS token = <|eot_id|>
		case pt_llama:
		{
			m_build_prompt  = "<|begin_of_text|>\n";
			for( const auto& item : m_context_history )
			{
				m_build_prompt += item;
			}
			m_build_prompt += "<|eot_id|>\n";
			break;
		}
		
		// mistral
		case pt_mistral:
		{
			for( const auto& item : m_context_history )
			{
				m_build_prompt += item;
			}
			break;
		}
		
		// straight text
		case pt_none:
		{
			for( const auto& item : m_context_history )
			{
				m_build_prompt += item;
			}
			break;
		}
	}// switch
	
	// debug
	cout << "Prompt type : " << m_prompt_type << endl;
	cout << endl;
	cout << "### Full prompt." << endl;
	cout << m_build_prompt << endl << endl;
}

// ***************
// init - Load

void lsLlamacpp::init_model()
{
	// debug
	cout << endl << "*************************" << endl;
	cout << "Loading Model : " << m_model_path << endl;
	
	// GPT defaults
	m_past = 0;
	// Lora adapters
	m_params.use_mmap = true;
	// model and context
	m_params.n_ctx = m_context_size;
	m_params.model = m_model_path;
	m_params.seed = time(NULL);
	// Prompt
	m_params.input_prefix_bos = false;
	m_params.input_prefix = "";
	m_params.input_suffix = "";
	// threads
	m_params.n_threads = thread::hardware_concurrency();
	// GPU layers
	m_params.n_gpu_layers = 99;
	cout << "n_gpu_layers = " << m_params.n_gpu_layers << endl;
	
	LOG("%s: load the model\n", __func__);
	llama_init_result llama_init = llama_init_from_gpt_params( m_params );
	m_model = llama_init.model;
	m_ctx = llama_init.context;
}

void lsLlamacpp::cleanup()
{
	if( m_ctx )
	{
		llama_free( m_ctx );
		m_ctx = NULL;
	}
	
	if( m_model )
	{
		llama_free_model( m_model );
		m_model = NULL;
	}
}

void lsLlamacpp::set_model( const string& path, const string& model_name )
{
	m_model_path = path;
	
	// database init
	m_chunkyDB.init( "lsChatAI (data) <data@lschatai.app>", "lschatai_data_" + model_name );
	m_memory.init( "lsChatAI (memory) <memory@lschatai.app>", "lschatai_memory_" + model_name );
	m_short_memory.init( "lsChatAI (memory) <memory@lschatai.app>", "lschatai_short_memory_" + model_name );
	
	// Load model.
	cleanup();
	init_model();
	
	if(! m_chunkyDB.empty() ) m_use_chunkyDB = true;
}

// ***********************
// database.

void lsLlamacpp::ingest( lsChatAI *chat )
{
	// debug
	// cout << "lsLlamacpp::ingest() - Called..." << endl;
	
	// Turn ChunkyEB on
	cout << "ChunkyDB: ON" << endl;
	m_use_chunkyDB = true;
	
	for( const string& data : m_data )
	{
		m_chunkyDB.insert( data, m_chunk_size );
	}
	
	// Tell chat we are done.
	if( chat ) chat->ingest_notify();
}

void lsLlamacpp::clear_db()
{
	m_chunkyDB.cleanup();
	m_chunkyDB.delete_data();
	// Turn ChunkyEB off
	cout << "ChunkyDB: OFF" << endl;
	m_use_chunkyDB = false;
}

bool lsLlamacpp::chunkyDB_empty()
{
	return m_chunkyDB.empty();
}

void lsLlamacpp::clear_memory()
{
	// debug
	cout << "Clearing memory..." << endl;
	
	// Short term memory.
	m_short_memory.cleanup();
	m_short_memory.delete_data();
	
	// Long term memory.
	m_memory.cleanup();
	m_memory.delete_data();
}

void lsLlamacpp::clear_context()
{
	llama_kv_cache_seq_rm( m_ctx, 0, -1, -1 );
	m_past = 0;
}

// ***********************
// Submit ****************

int lsLlamacpp::submit( lsChatAI *chat )
{
	// User stop
	m_stop = false;
	// Clear context. clears kv cache and sets m_past to 0
	clear_context();
	
	// Check model is loaded
	if( m_model == NULL )
	{
		cerr << __func__ << " - ERROR: No model loaded..." << endl;
		// notify chat window
		chat->notify();
		return 1;
    }
	
	if( m_prompt.empty() )
	{
		chat->add_text( "\nPrompt cant be empty...\nTry saying 'Hello'.\n" );
		chat->write();
		cerr << __func__ << "ERROR: submit - prompt empty..." << endl;
		// notify chat window
		chat->notify();
		return 1;
	}
	
	// Build prompt
	// debug
	cout << __func__ << " : build and check prompt size..." << endl;
	
	// build and check prompt size
	// Adds memory, database chunks
	build_prompt( chat );
	m_params.prompt = m_build_prompt;
	if( m_params.prompt.empty() )
	{
		chat->add_text( "\nPrompt cant be empty...\nTry saying 'Hello'.\n" );
		chat->write();
		// Tell chat we are done
		chat->notify();
		return 1;
	}
	submit_chunk( chat );
	
	// Tell chat we are done
	chat->notify();
	return 0;
}

int lsLlamacpp::submit_chunk( lsChatAI *chat )
{
	// debug
	cout << __func__ << " - called..." << endl;
	
	// sparams - temperature etc.
	llama_sampling_params& sparams = m_params.sparams;
	// temperature
	sparams.temp = m_temperature;
	
    const int n_ctx_train = llama_n_ctx_train( m_model );
    const int n_ctx = llama_n_ctx( m_ctx );
    LOG("n_ctx: %d\n", n_ctx);
	if( n_ctx > n_ctx_train )
	{
        LOG_TEE( "%s: warning: model was trained on only %d context tokens (%d specified)\n", __func__, n_ctx_train, n_ctx );
    }
	
	// debug
	cout << __func__ << " : tokenize the prompt..." << endl;
	
	const bool add_bos = llama_should_add_bos_token( m_model );
	LOG("add_bos: %d\n", add_bos);
	vector<llama_token> embd_inp;
	embd_inp = llama_tokenize( m_ctx, m_params.prompt, add_bos, true );
	
	// debug log file.
	LOG("prompt: \"%s\"\n", log_tostr( m_params.prompt ) );
	LOG( "tokens: %s\n", LOG_TOKENS_TOSTR_PRETTY( m_ctx, embd_inp ).c_str() );
	
	// Should not run without any tokens
	if( embd_inp.empty() )
	{
		embd_inp.push_back( llama_token_bos( m_model ) );
		LOG( "embd_inp was considered empty and bos was added: %s\n", LOG_TOKENS_TOSTR_PRETTY(m_ctx, embd_inp).c_str() );
	}
	
	// TODO: does this work?
	m_past = embd_inp.size();
	
	// Response
	chat->add_text( "\n\n### Response\n" );
	chat->write();
	fprintf( stdout, "\n\n### Response\n" );
	fflush(stdout);
	
	// main loop
	int n_remain	= m_params.n_predict;
	int n_consumed	= 0;
	
	vector<llama_token> embd;
	struct llama_sampling_context *ctx_sampling = llama_sampling_init( sparams );
	
	// Short term memory.
	string ai_response;
	
	while( n_remain != 0 )
	{
		// predict
		if(! embd.empty() )
		{
			// Linit.
			int max_embd_size = n_ctx - 4;
			
			/* debug
			cout << __func__ << " - max_embd_size = " << max_embd_size << endl;
			cout << __func__ << " - embd.size() = " << embd.size() << endl;
			cout << __func__ << " - m_past = " << m_past << endl;
			*/
			
			// Ensure the input doesn't exceed the context size by truncating embd if necessary.
			if( (int) embd.size() > max_embd_size )
			{
				const int skipped_tokens = (int) embd.size() - max_embd_size;
				embd.resize( max_embd_size );
				
				printf("<<input too long: skipped %d token%s>>", skipped_tokens, skipped_tokens != 1 ? "s" : "");
				fflush(stdout);
			}
			
			// Context check
			if( m_past + (int) embd.size() > n_ctx )
			{
				// Context full
				cout << endl << __func__ << " - Context full, used tokens = " << m_past + (int) embd.size() << endl;
				// Tell the user.
				chat->add_text( "\n\n*** AI context full. Please clear the history." );
				chat->write();
				return 1;
			}
			
            for( int i = 0; i < (int) embd.size(); i += m_params.n_batch )
			{
                int n_eval = (int) embd.size() - i;
				
                if( n_eval > m_params.n_batch )
				{
					n_eval = m_params.n_batch;
				}
				LOG("eval: %s\n", LOG_TOKENS_TOSTR_PRETTY( m_ctx, embd ).c_str());
				// debug
				// cout << __func__ << " - eval : " << LOG_TOKENS_TOSTR_PRETTY( m_ctx, embd ).c_str() << endl;
				
				if( llama_decode( m_ctx, llama_batch_get_one( &embd[i], n_eval, m_past, 0 ) ) )
				{
					// Tell user.
					chat->add_text( "\nAI context full. Please clear the history." );
					chat->write();
					
					LOG_TEE("\n%s : failed to eval\n", __func__);
					cout << __func__ << " - context used : " << m_past + (int) embd.size() << endl;
					return 1;
				}
				
				m_past += n_eval;
				LOG( "m_past = %d\n", m_past );
			}// for
			
		}// if ! embed.empty()
		
		embd.clear();
		
		if( (int) embd_inp.size() <= n_consumed )
		{
			const llama_token id = llama_sampling_sample( ctx_sampling, m_ctx, 0 );
			llama_sampling_accept( ctx_sampling, m_ctx, id, true );
			
			LOG("last: %s\n", LOG_TOKENS_TOSTR_PRETTY( m_ctx, ctx_sampling->prev).c_str());
			
			// Store token
			embd.push_back( id );
			
			// decrement remaining sampling budget
			--n_remain;
			
			LOG( "n_remain: %d\n", n_remain );
        } else
		{
            // some user input remains from prompt, forward it to processing
            LOG( "embd_inp.size(): %d, n_consumed: %d\n", (int) embd_inp.size(), n_consumed );
            while( (int) embd_inp.size() > n_consumed )
			{
                embd.push_back( embd_inp[n_consumed] );
				
				// push the prompt in the sampling context in order to apply repetition penalties later
				// for the prompt, we don't apply grammar rules
				llama_sampling_accept( ctx_sampling, m_ctx, embd_inp[n_consumed], false );
				
                ++n_consumed;
                if( (int) embd.size() >= m_params.n_batch )
				{
                    break;
				}
			}// while embd_inp
		}
		
        // display text
		for( auto id : embd )
		{
			// Ignore control tokens.
			if(! llama_token_is_control( llama_get_model(m_ctx), id ) )
			{
				const string token_str = llama_token_to_piece( m_ctx, id );
				// debug
				printf( "%s", token_str.c_str() );
				
				if( embd.size() > 1 )
				{
					// Input tokens.
				} else
				{
					// build ai_response for short memory.
					ai_response += token_str;
					
					// Output tokens.
					chat->add_text( token_str );
					chat->write();
				}
			}
		}
		fflush(stdout);
		
        // Check for EOS, end of stream token.
        if(! embd.empty() && embd.back() == llama_token_eos( m_model ) )
		{
			cout << "[EOS]" << endl;
            break;
        }
		
		if( m_stop )
		{
			cout << "[USER STOP]" << endl;
			break;
		}
		
    }// while - main loop
	
	// Short term Memory
	// Stores a pre=fomated chunk of user prompt and responce.
	string tmp;
	switch( m_prompt_type )
	{
		// Llama 3.1
		case pt_llama:
		{
			tmp  = m_prompt;
			tmp +=  "\n";
			tmp += ai_response;
			tmp += "\n";
			break;
		}
		
		// mistral
		case pt_mistral:
		{
			tmp = "\n<s>[INST] " + m_prompt + " [/INST]\n";
			tmp += ai_response;
			tmp += "</s>";
			break;
		}
		
		// straight text
		case pt_none:
		{
			tmp  = m_prompt;
			tmp +=  "\n";
			tmp += ai_response;
			tmp += "\n";
			break;
		}
	}// switch
	if(! tmp.empty() )
	{
		// Add short term memory
		m_short_memory.insert( tmp, m_chunk_size );
	}
	
	// response finished.
	chat->add_text( "\n\n" );
	chat->write();
    LOG_TEE("\n\n");
	
	// cleanup
	llama_sampling_free( ctx_sampling );
	
	return 0;
}


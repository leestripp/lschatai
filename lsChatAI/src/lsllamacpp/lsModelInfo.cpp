#include "lsModelInfo.h"

lsModelInfo::lsModelInfo()
{
	m_prompt_type = 0;
	m_context_size = 4096;
	// Chunky DB
	m_use_chunkyDB = true;
	m_max_db_chunks = 2;
	m_chunk_size = 500;
	// memory
	m_max_memory = 2;
}

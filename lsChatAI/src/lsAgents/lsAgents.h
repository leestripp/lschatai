#ifndef LSAGENTS_H
#define LSAGENTS_H

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <tuple>
#include <thread>
#include <mutex>

// Agent Base class
#include "lsAgent_Base.h"

using namespace std;

class lsAgents
{
public:
	lsAgents();
	virtual ~lsAgents();
	
	void run( const string& prompt );
	// agent response
	void add_response( const string& response );
	void add_link( const tuple<string, string>& link );
	
	void add_agent( lsAgent_Base* agent );
	
	// Inject context responses
	vector<string> m_agent_responses;
	// reference links.
	bool m_has_links;
	vector<tuple<string, string>> m_links;
	
private:
	// Utils
	vector<string> split( string str, const string& token );
	vector<lsAgent_Base *>	m_agent_list;
	// Threads
	mutex m_agent_mutex;
};

#endif // LSAGENTS_H

#include "lsAgent_Weather.h"
#include "lsAgents.h"

// lsCurl
#include <lsCurl/lsCurl.h>
// Json
#include <json/json.h>


lsAgent_Weather::lsAgent_Weather( lsAgents *am ) : lsAgent_Base(am)
{
	if(! m_settings.load( get_base_dir() + "/config/weather.config" ) )
	{
		m_settings.add_entry( "api_key", "[API_KEY]" );
		m_settings.print();
		// save
		m_settings.save();
	}
}

lsAgent_Weather::~lsAgent_Weather()
{
}

void lsAgent_Weather::run( const vector<string>& prompt_words )
{
	if( prompt_words.empty() ) return;
	
	// Find keywords
	// vector<string> find( const vector<string>& words, const string& word );
	vector<string> result;
	
	result = find( prompt_words, "weather:" );
	if(! result.empty() )
	{
		lsCurl lscurl;
		string readBuffer;
		
		// curl "https://api.openweathermap.org/data/2.5/weather?q=Budapest&appid=APIKEY&units=metric"
		string urlStr;
		urlStr = "https://api.openweathermap.org/data/2.5/weather";
		urlStr += "?q=";
		for( ulong i=0; i<result.size(); i++ )
		{
			urlStr += result[i];
			if( i < result.size() - 1 ) urlStr += "%20";
		}
		urlStr += "&appid=" + m_settings.get_value( "api_key" );
		urlStr += "&units=metric";
		// debug
		cout << "lsAgent_Weather::run URL : " << urlStr << endl;
		
		lscurl.m_mode = LSM_GET;
		lscurl.m_url = urlStr;
		lscurl.start();
		// wait.
		lscurl.join();
		
		readBuffer = lscurl.get_pageBuffer();
		if(! readBuffer.empty() )
		{
			// debug
			cout << "readBuffer : " << readBuffer << endl;
			
			// Json parse
			const auto readBufferLength = static_cast<int>( readBuffer.length() );
			JSONCPP_STRING err;
			Json::Value root;
			Json::Value jmain;
			Json::Value jweather;
			
			Json::CharReaderBuilder builder;
			const unique_ptr<Json::CharReader> reader( builder.newCharReader() );
			if(! reader->parse( readBuffer.c_str(), readBuffer.c_str() + readBufferLength, &root, &err) )
			{
				cerr << "ERROR: unable to parse JSON" << endl;
				return;
			}
			jmain = root["main"];
			jweather = root["weather"];
			
			// debug
			// Json::StreamWriterBuilder wbuilder;
			// wbuilder.settings_["indentation"] = "\t";
			// unique_ptr<Json::StreamWriter> writer( wbuilder.newStreamWriter() );
			// writer->write( root, &cout );
			// cout << endl << "Size: " << root.size() << endl;
			
			stringstream ss;
			ss << "description: " << jweather[0]["description"] << endl;
			ss << "temp: " << jmain["temp"] << endl;
			ss << "feels_like: " << jmain["feels_like"] << endl;
			ss << "humidity: " << jmain["humidity"] << endl;
			ss << "pressure: " << jmain["pressure"] << endl;
			
			// add response to agent manager buffer.
			m_agent_manager->add_response( ss.str() );
		}
	}
}


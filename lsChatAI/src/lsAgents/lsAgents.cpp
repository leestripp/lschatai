#include "lsAgents.h"

lsAgents::lsAgents()
{
	// reference links.
	m_has_links = false;
}

lsAgents::~lsAgents()
{
	// cleanup
	for( auto agent : m_agent_list )
	{
		delete agent;
	}
}

// *********************
// Run.

void lsAgents::run( const string& prompt )
{
	if( prompt.empty() ) return;
	
	// clear old data.
	m_agent_responses.clear();
	// reference links.
	m_has_links = false;
	m_links.clear();
	
	vector<string> prompt_words;
	
	// grab first line.
	stringstream ss( prompt );
	string first_line;
	getline( ss, first_line );
	if( first_line.empty() ) return;
	
	// split line into words.
	prompt_words = split( first_line, " " );
	if( prompt_words.empty() ) return;
	
	vector<thread> th;
	ulong threads = thread::hardware_concurrency();
	ulong tc = 0;
	
	// run agents
	for( auto agent : m_agent_list )
	{
        // Add thread
        th.push_back( thread( &lsAgent_Base::run, agent, prompt_words ) );
        tc++;
		
        // check our thread count
		if( tc >= threads )
		{
			// debug
			cout << "lsAgents::run - Thread count : " << tc << endl;
			
			// Join and wait
			for( auto &t : th )
			{
				t.join();
			}
			// clean up
			th.clear();
			tc = 0;
		}
	}
	
	// last running threads.
	if(! th.empty() )
	{
		// debug
		cout << "lsAgents::run - Thread count : " << tc << endl;
		
		// Join and wait
		for( auto &t : th )
		{
			t.join();
		}
	}
}

// Responce.

void lsAgents::add_response( const string& responce )
{
	// lock
	m_agent_mutex.lock();
	m_agent_responses.push_back( responce );
	m_agent_mutex.unlock();
}

void lsAgents::add_link( const tuple<string, string>& link )
{
	// lock
	m_agent_mutex.lock();
	m_links.push_back( link );
	m_has_links = true;
	m_agent_mutex.unlock();
}

// Agent list

void lsAgents::add_agent( lsAgent_Base* agent )
{
	m_agent_list.push_back( agent );
}


// Utilities

vector<string> lsAgents::split( string str, const string& token )
{
    vector<string>result;
	
	if( str.empty() )
	{
		cerr << "ERROR: lsAgents::split : empty string" << endl;
		return result;
	}
	
    while( str.size() )
	{
        ulong index = str.find( token );
        if( index != string::npos )
		{
            result.push_back(str.substr(0,index));
            str = str.substr(index+token.size());
            if( str.size() == 0 )
			{
				result.push_back( str );
			}
        } else
		{
            result.push_back( str );
            str = "";
        }
    }
	
    return result;
}

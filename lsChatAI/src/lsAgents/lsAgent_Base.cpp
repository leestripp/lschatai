#include "lsAgent_Base.h"

lsAgent_Base::lsAgent_Base( lsAgents *am )
{
	m_agent_manager = am;
	// Get base directory.
	m_base_dir = getenv( "HOME" );
	m_base_dir += "/.lsChatAI";
	
	// debug
	cout << "Agent base dir : " << m_base_dir << endl;
	
}

lsAgent_Base::~lsAgent_Base()
{
}


vector<string> lsAgent_Base::find( const vector<string>& words, const string& word )
{
	vector<string> result;
	bool capture = false;
	
	for( const auto& w : words )
	{
		// Check end of command.
		if( capture && check_command(w) ) break;
		
		if( w == word )
		{
			capture = true;
			
		} else if( capture )
		{
			result.push_back( w );
		}
	}
	return result;
}

bool lsAgent_Base::check_command( const string& cmd )
{
	if( cmd[cmd.size()-1] == ':' ) return true;
	return false;
}

string lsAgent_Base::get_base_dir()
{
	return m_base_dir;
}

#ifndef LSAGENT_DATETIME_H
#define LSAGENT_DATETIME_H

#include <iostream>
#include <ctime>
#include <iomanip>

// Base class
#include "lsAgent_Base.h"

using namespace std;

class lsAgent_DateTime : public lsAgent_Base
{
public:
	lsAgent_DateTime( lsAgents *am );
	virtual ~lsAgent_DateTime();
	
	void run( const vector<string>& prompt_words );
	
};

#endif // LSAGENT_DATETIME_H



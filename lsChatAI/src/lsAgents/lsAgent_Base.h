#ifndef LSAGENT_BASE_H
#define LSAGENT_BASE_H

#include <iostream>
#include <vector>

// C
#include <stdlib.h>

// protos
class lsAgents;

using namespace std;

class lsAgent_Base
{
public:
	lsAgent_Base( lsAgents *am );
	virtual ~lsAgent_Base();
	
	virtual void run( const vector<string>& prompt_words ) = 0;
	// utils
	vector<string> find( const vector<string>& words, const string& word );
	bool check_command( const string& cmd );
	string get_base_dir();
	
	lsAgents *m_agent_manager;
	
private:
	string m_base_dir;
};

#endif // LSAGENT_BASE_H


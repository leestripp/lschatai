
#ifndef LSAGENT_WEATHER_H
#define LSAGENT_WEATHER_H

#include <iostream>
#include <iomanip>
// lsConfig
#include <lsConfig/lsConfig.h>

// Base class
#include "lsAgent_Base.h"

using namespace std;

class lsAgent_Weather : public lsAgent_Base
{
public:
	lsAgent_Weather( lsAgents *am );
	virtual ~lsAgent_Weather();
	
	void run( const vector<string>& prompt_words );
	
private:
	lsConfig m_settings;
	string m_api_key;
};

#endif // LSAGENT_WEATHER_H



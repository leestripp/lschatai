#include "lsAgent_Search.h"
#include "lsAgents.h"

// lsCurl
#include <lsCurl/lsCurl.h>
// Json
#include <json/json.h>

lsAgent_Search::lsAgent_Search( lsAgents *am ) : lsAgent_Base(am)
{
	if(! m_settings.load( get_base_dir() + "/config/search.config" ) )
	{
		m_settings.add_entry( "api_key", "[API_KEY]" );
		m_settings.add_entry( "app_id", "[APP_ID]" );
		m_settings.print();
		// save
		m_settings.save();
	}
}

lsAgent_Search::~lsAgent_Search()
{
}


void lsAgent_Search::run( const vector<string>& prompt_words )
{
	if( prompt_words.empty() ) return;
	
	// settings
	string api_key = m_settings.get_value( "api_key" );
	string app_id = m_settings.get_value( "app_id" );
	
	// Find keywords
	// vector<string> find( const vector<string>& words, const string& word );
	vector<string> result = find( prompt_words, "search:" );
	if(! result.empty() )
	{
		lsCurl lscurl;
		string readBuffer;
		
		string urlStr;
		urlStr = "https://www.googleapis.com/customsearch/v1";
		urlStr += "?key=" + api_key;
		urlStr += "&cx=" + app_id;
		urlStr += "&num=5";
		urlStr += "&q=";
		for( ulong i=0; i<result.size(); i++ )
		{
			urlStr += result[i];
			if( i < prompt_words.size() - 1 ) urlStr += "%20";
		}
		// debug
		cout << "lsAgent_Search::run URL : " << urlStr << endl;
		
		lscurl.m_mode = LSM_GET;
		lscurl.m_url = urlStr;
		lscurl.start();
		// wait.
		lscurl.join();
		
		readBuffer = lscurl.get_pageBuffer();
		if(! readBuffer.empty() )
		{
			// debug
			// cout << "readBuffer : " << readBuffer << endl;
			
			// Json parse
			const auto readBufferLength = static_cast<int>( readBuffer.length() );
			JSONCPP_STRING err;
			Json::Value root;
			Json::Value items;
			
			Json::CharReaderBuilder builder;
			const unique_ptr<Json::CharReader> reader( builder.newCharReader() );
			if(! reader->parse( readBuffer.c_str(), readBuffer.c_str() + readBufferLength, &root, &err) )
			{
				cerr << "ERROR: unable to parse JSON" << endl;
				return;
			}
			items = root["items"];
			
			// debug
			// Json::StreamWriterBuilder wbuilder;
			// wbuilder.settings_["indentation"] = "\t";
			// std::unique_ptr<Json::StreamWriter> writer( wbuilder.newStreamWriter() );
			// writer->write( root, &cout );
			// cout << endl << "Size: " << root.size() << endl;
			
			stringstream ss;
			for( Json::Value::ArrayIndex i = 0; i < items.size(); i++ )
			{
				// Add response text.
				ss << "text: " << items[i]["htmlSnippet"] << endl;
				ss << "title: " << items[i]["htmlTitle"] << endl;
				ss << "url: " << items[i]["link"] << endl;
				
				// Add ref links.
				Json::StreamWriterBuilder builder;
				builder["indentation"] = ""; // If you want whitespace-less output
				string title = Json::writeString( builder, items[i]["htmlTitle"] );
				title.erase( std::remove_if( title.begin(), title.end(),
						[](unsigned char c) {
						if(c=='\"' || c=='\'')
							return true;
						else
							return false; } ), title.end() );
				string link = Json::writeString( builder, items[i]["link"] );
				link.erase( std::remove_if( link.begin(), link.end(),
						[](unsigned char c) {
						if(c=='\"' || c=='\'')
							return true;
						else
							return false; } ), link.end() );
				m_agent_manager->add_link( make_tuple( title, link ) );
			}
			// add response to agent manager buffer.
			m_agent_manager->add_response( ss.str() );
		}
	}
}


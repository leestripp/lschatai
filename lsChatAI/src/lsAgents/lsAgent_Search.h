#ifndef LSAGENT_SEARCH_H
#define LSAGENT_SEARCH_H

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <string>
// lsConfig
#include <lsConfig/lsConfig.h>

// Base class
#include "lsAgent_Base.h"

using namespace std;

class lsAgent_Search : public lsAgent_Base
{
public:
	lsAgent_Search( lsAgents *am );
	virtual ~lsAgent_Search();
	
	void run( const vector<string>& prompt_words );
	
private:
	lsConfig m_settings;
	string m_api_key;
	string m_app_id;
};

#endif // LSAGENT_SEARCH_H



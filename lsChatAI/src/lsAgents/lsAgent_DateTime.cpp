#include "lsAgent_DateTime.h"
#include "lsAgents.h"


lsAgent_DateTime::lsAgent_DateTime( lsAgents *am ) : lsAgent_Base(am)
{
}

lsAgent_DateTime::~lsAgent_DateTime()
{
}

void lsAgent_DateTime::run( const vector<string>& prompt_words )
{
	if( prompt_words.empty() ) return;
	
	// vector<string> find( const vector<string>& words, const string& word );
	vector<string> result;
	
	result = find( prompt_words, "datetime:" );
	if(! result.empty() )
	{
		if( result[0] == "now" )
		{
			stringstream ss;
			time_t t = time( nullptr );
			tm tm = *localtime( &t );
			ss << "The current time is " << put_time( &tm, "%c %Z" ) << endl;
			
			// add response to agent manager buffer.
			m_agent_manager->add_response( ss.str() );
		}
	}
}

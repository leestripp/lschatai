#include "ChatWindow.h"

int main( int argc, char* argv[] )
{
	auto app = Gtk::Application::create("com.lschatai.leestripp");
	return app->make_window_and_run<ChatWindow>(argc, argv);
}


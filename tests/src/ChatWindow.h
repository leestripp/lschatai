#ifndef CHATWONDOW_H
#define CHATWONDOW_H

#include <iostream>
// gtkmm
#include <gtkmm.h>

#include "lsChatAI.h"

using namespace std;

class ChatWindow : public Gtk::Window
{
public:
	ChatWindow();
	virtual ~ChatWindow();
	
private:
	lsChatAI m_chatAI;
};

#endif


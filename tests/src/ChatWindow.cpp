#include "ChatWindow.h"

ChatWindow::ChatWindow()
{
	set_title( "lsChatAI" );
	set_default_size( 800, 900 );
	
	set_child( m_chatAI );
	
	// set main window
	m_chatAI.set_MainWindow( this );
}

ChatWindow::~ChatWindow()
{
}


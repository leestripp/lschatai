# lsChatAI v0.7-Alpha

![image](screenshots/lsChatAI.png)

## Features:
* Models run locally.
* Automatic model download.
* Import data (encrypted chunks on disk)
  * Text document import (.txt).
  * Webpage import.
  * All data stored locally.
  * You can Clear Imported Data, this will delete the data from disk.
* Memory: Prompts and Agents output are encrypted on disk for future lookup.
  * Clear Memory button clears that data.
* Built as a Static library. MIT License.
  * Library: liblsChatAI_gtkmm.a is a widget for gtkmm4.
  * The tests folder shows how to use the widget in your own gtkmm4 apps. The standalone app lsChatAI, is also in tests.
* Threaded Agents.
  * Examples: datetime, weather and search.
* Built using the llama.cpp API
  * AMD and Nvidia GPU support. (If you can get llama.cpp to build with GPU support.)
* Courage (temperature) slider.

## Using Agents
* Google search : use "search: [search ...]"
* Weather : use "weather: [Town]"
* time : use "datetime: now"

Make sure your Agent commands are all in the first line.
e.g.
```
datetime: now search: why is there glue on my pizza? weather: Hervey Bay
Create an overview of what you fond on the web.
```

## Recomended models
* mistral-7b-instruct-v0.3.Q4_K_M.gguf
* Meta-Llama-3.1-8B-Instruct-Q4_K_M.gguf

# Requirements.
* llama.cpp
* libcurl
* jsoncpp
* gpgme
* gtkmm 4
* cmake

# Install
## llama.cpp and lsChatAI
```
cd ~/
mkdir temp
cd temp
git clone https://gitlab.com/leestripp/llamacpp.git
mkdir ./llamacpp/build
cd llamacpp/build
cmake -DCMAKE_INSTALL_PREFIX=~/.local/llamacpp -DCMAKE_BUILD_TYPE=Release ..
make
make install

cd ~/temp

git clone https://gitlab.com/leestripp/lschatai.git
mkdir ./lschatai/build
cd lschatai/build
cmake ..
make
make install
```
lsChatAI is installed into your home directory. "~/.local/bin"

Run:
```
lschatai_start
```
OR

Run it from your Linux desktop menu system.

## directory layout.
```
~/.lsChatAI
├── config
│   ├── search.config
│   └── weather.config
├── mistral-7b-32K.lmc
└── models
    └── mistral-7b-instruct-v0.2.Q4_K_M.gguf
```

## weather config file
```
# lsConfig v0.1
api_key=[APP_ID]
```

## Google search config file
```
# lsConfig v0.1
api_key=[API_KEY]
app_id=[APP_ID]
```

## Example Llama Model Config (.lmc) file.
```
# lsChatAI : llama model config
name=mistral-7b-32K
model=models/mistral-7b-instruct-v0.2.Q4_K_M.gguf
context_size=32768
prompt_type=2
max_memory=2
max_db_chunks=1
use_chunkyDB=false
chunk_size=4000
# lora=[model]
```
